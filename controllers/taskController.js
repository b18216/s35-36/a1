
const Task = require("../models/Task");

module.exports.createTaskController=(req, res)=>{

    Task.findOne({name: req.body.name})
    .then(result =>{
        if(result !== null && result.name === req.body.name){
            return res.send('Duplicate Task')
        } else {
            let newTask = new Task({
                name: req.body.name,
                status: req.body.status
            })
            newTask.save()
                .then(result =>res.send(result))
                .catch(err => res.send(err));
        }
    })
    .catch(err=>res.send(err));
};

module.exports.getAllTaskController = (req,res)=>{
    Task.find({})
    .then(result =>res.send(result))
    .catch(err =>res.send(err));
};

module.exports.getSingleTaskController = (req, res)=>{
    Task.findById(req.params.id)
    .then(result => res.send(result))
    .catch(err => res.send(err));

}

module.exports.updateTaskStatusController = (req, res)=>{
    let updates = {
        status:req.body.status
    };
    Task.findByIdAndUpdate(req.params.id, updates, {new:true})
    .then(updatedTask => res.send(updatedTask))
    .catch(err => res.send(err))
};