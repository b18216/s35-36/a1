const Task = require('../models/Task');
const User = require('../models/Users');

module.exports.creatUserController = (req, res)=>{
    
    User.findOne({username: req.body.username})
    .then(result=>{

        if(result !== null && result.username === req.body.username){
            return res.send("Duplicate User Found")
        } else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })
            newUser.save()
            .then(result => res.send(result))
            .catch(err =>res.send(error));
        }
    })
    .catch(err =>res.send(err));

}

module.exports.getAllUserController =(req,res)=>{
    User.find({})
    .then(result => res.send(result))
    .catch(err => res.send(err));
}
module.exports.getSingleUserController = (req, res)=>{
    User.findById(req.params.id)
    .then(result =>res.send(result))
    .catch(err =>res.send(err));
};

module.exports.updateUserUsernameController =(req, res)=>{
    let update = {username:req.body.username};
    User.findByIdAndUpdate(req.params.id, update, {new:true})
    .then(updatedUser => res.send(updatedUser))
    .catch(err => res.send(err));
};