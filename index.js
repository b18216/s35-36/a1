

const express = require('express');
// allows us to connect to mongodb
const mongoose = require('mongoose');
const port = 4000;
const app = express();

// mongoose connection
// get SRV string from mongodb
// .net/___? add name of DB
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mvn65.mongodb.net/task182?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

let db = mongoose.connection;

db.on('error',console.error.bind(console,"DB Connection Error"));
db.once('open', ()=>console.log("Successfully Connected to MongoDB"));

// MIDDLEWARES
app.use(express.json());
// reading data forms
// usually string or array are being accepted, with this middleware. this will enable us to accept other data type
app.use(express.urlencoded({extended:true}))

// Routes
const taskRoutes = require('./routes/taskRoutes')
app.use('/task', taskRoutes);

const userRoutes =require('./routes/userRoutes')
app.use('/user',userRoutes);
// grouping routes
// port listener
app.listen(port, ()=> console.log(`Server is running at port: ${port}`));