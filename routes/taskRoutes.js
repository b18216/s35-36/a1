const express = require('express');
const router = express.Router();

const taskController = require('../controllers/taskController');

router.post('/', taskController.createTaskController);

router.get('/', taskController.getAllTaskController);

router.get('/getSingleTask/:id',taskController.getSingleTaskController);

router.put('/updateTask/:id', taskController.updateTaskStatusController);
module.exports = router;