const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');

router.post('/',userController.creatUserController);


router.get('/',userController.getAllUserController);


router.get('/getSingleUser/:id',userController.getSingleUserController );

router.put('/updateUser/:id', userController.updateUserUsernameController)
module.exports = router;