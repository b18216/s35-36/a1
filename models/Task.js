const mongoose = require('mongoose');
// schema - blueprint for out data/document
const taskSchema = new mongoose.Schema({
    name: String,
    status: String
});
// mongoose model = mongoose.model(----)
module.exports = mongoose.model("Task", taskSchema)

